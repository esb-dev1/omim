#include "Framework.hpp"

#include "com/mapswithme/core/jni_helper.hpp"
#include "map/gps_tracker.hpp"

#include <chrono>

namespace
{

::Framework * frm()
{
  return (g_framework ? g_framework->NativeFramework() : nullptr);
}

}  // namespace

extern "C"
{
  JNIEXPORT void JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeSetEnabled(JNIEnv * env, jclass clazz, jboolean enable)
  {
    GpsTracker::Instance().SetEnabled(enable);
    Framework * const f = frm();
    if (f == nullptr)
      return;
    if (enable)
      f->ConnectToGpsTracker();
    else
      f->DisconnectFromGpsTracker();
  }

  JNIEXPORT jboolean JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeIsEnabled(JNIEnv * env, jclass clazz)
  {
    return GpsTracker::Instance().IsEnabled();
  }

  JNIEXPORT void JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeSetDuration(JNIEnv * env, jclass clazz, jint durationHours)
  {
    GpsTracker::Instance().SetDuration(std::chrono::hours(durationHours));
  }

  JNIEXPORT jint JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeGetDuration(JNIEnv * env, jclass clazz)
  {
    return static_cast<jint>(GpsTracker::Instance().GetDuration().count());
  }

  int getAlt(std::vector<location::GpsInfo> toAdd, location::GpsTrackInfo p) {
      double d = std::numeric_limits<double>::max();
      double a = 0;
      for (auto const &ip : toAdd) {
          double de = mercator::DistanceOnEarth(mercator::FromLatLon(ip.m_latitude, ip.m_longitude), mercator::FromLatLon(p.m_latitude, p.m_longitude));
          if (de < d) {
            d = de;
            a = ip.m_altitude;
          }
      }
      return a;
  }

  JNIEXPORT void JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeSaveTrack(JNIEnv * env, jclass clazz, jstring name)
  {
    ::Framework *f = frm();

    std::vector<location::GpsInfo> toAdd;
    GpsTracker::Instance().GetTrack()->GetPoints(toAdd);
    if(toAdd.empty())
      return;

    std::vector<location::GpsTrackInfo> outPoints;

    GpsTrackFilter filter;
    filter.Process(toAdd, outPoints);

    kml::TrackData data;

    for (auto const & ip : outPoints)
    {
      geometry::PointWithAltitude pt(mercator::FromLatLon(ip.m_latitude, ip.m_longitude), getAlt(toAdd, ip));
      data.m_pointsWithAltitudes.push_back(pt);
    }
    if(data.m_pointsWithAltitudes.size() < 2) {
        GpsTracker::Instance().GetTrack()->Clear();
        f->GetDrapeEngine()->ClearGpsTrackPoints();
        return;
    }

    kml::TrackLayer layer;
    layer.m_color.m_rgba = 0x0000ffff; // dp::Color(0, 0, 255, 255)
    data.m_layers.push_back(layer);
    kml::SetDefaultStr(data.m_name, jni::ToNativeString(env, name));

    Track const *t = f->CreateTrack(std::move(data));

    GpsTracker::Instance().GetTrack()->Clear();
    f->GetDrapeEngine()->ClearGpsTrackPoints();

    // f->ShowTrack(*t);
  }

  JNIEXPORT void JNICALL
  Java_com_mapswithme_maps_location_TrackRecorder_nativeClearTrack(JNIEnv * env, jclass clazz)
  {
    GpsTracker::Instance().GetTrack()->Clear();
    frm()->GetDrapeEngine()->ClearGpsTrackPoints();
  }
}
