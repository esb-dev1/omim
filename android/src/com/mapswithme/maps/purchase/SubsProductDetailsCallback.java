package com.mapswithme.maps.purchase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.List;

class SubsProductDetailsCallback
    extends StatefulPurchaseCallback<BookmarkPaymentState, BookmarkPaymentFragment>
    implements PlayStoreBillingCallback
{
  @Override
  void onAttach(@NonNull BookmarkPaymentFragment bookmarkPaymentFragment)
  {
  }
}
